'use strict';

var mongoose = require('mongoose'),
Schema = mongoose.Schema,
ObjectId = Schema.ObjectId,
l=require('../config/lib');

var fields = {
	username: { type: String },
	password: { type: String },
	firstname: { type: String },
	lastname: { type: String },
	emailid: { type: String },
	activationcode:{type:String},
	status:{type:String,default:'pending'} //p,a,s
			
};

var userSchema = new Schema(fields);

//JWT
/*var jwt = require('jsonwebtoken');

userSchema.methods.generateJwt = function() {
  var expiry = new Date();
  expiry.setDate(expiry.getDate() + 7);

  return jwt.sign({
    _id: this._id,
    email: this.email,
    name: this.name,
    exp: parseInt(expiry.getTime() / 1000),
  }, "saheb123456789"); // DO NOT KEEP YOUR SECRET IN THE CODE!
}; */

module.exports = mongoose.model('User', userSchema);
