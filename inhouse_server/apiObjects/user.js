// Module dependencies.
var crypto = require('crypto');
var mongoose = require('mongoose'),
User = mongoose.models.User,
api = {},
l=require('../config/lib');

/*
========= [ CORE METHODS ] =========
*/

// ALL
api.getAllUsers = function (skip,limit,cb) {
  var q=User.find({},'username emailid');
  
  if(skip!=undefined)
    q.skip(skip*1);

  if(limit!=undefined)
    q.limit(limit*1); 

  return q.exec(function(err, users) {
    cbf(cb,err,users);    
  });
};

// GET
api.getUser = function (id,cb) {

  User.findOne({ '_id': id }, function(err, user) {
    cbf(cb,err,user);
  });
};

api.activate = function (activationcode, cb) {
  User.findOne({ 'activationcode': activationcode }, function(err, user) {
    l.p('users for activation ::: ' + user);
    if(user == null){
      cbf(cb,"Activation link is incorrect",user);
    } else{
      user["status"] = "active";
    }
    return user.save(function (err) {
    cbf(cb,err,"Your account has been activated"); 
    }); //eo user.save
  });
};


api.addUser = function (user,cb) {
  if(user == 'undefined'){
    cb('No User Provided. Please provide valid user data.');
  }
 
  User.findOne({ 'username': user.username}, function(err, data) {
    var newuser = false;
    
    if(data != null){
      l.p('data frim apiObjects::::',data);
      cbf(cb,err,"Username already exist",newuser);
      //cbf(cb,err,"User already exist",newuser);
    } else{
        User.findOne({ 'emailid':user.emailid }, function(err, data) {
          l.p('checking for user duplicacy ::: '+ data);
          if(data != null){
            l.p('EmailID alredy exist.... ' , newuser);
            // cbf(cb,err,data,newuser);
             cbf(cb,err,"EmailID already exist",newuser);
          } else{
               l.p('creating new userqwert');
                user = new User(user);
                newuser = true;
                l.p('creating new user', user);
                user.activationcode  = genRandomString(16);
                var plainPassword = user.password;
                user.password = saltHashPassword(user.password);

                user.save(function (err) {
                  l.p('password zone12345');
                  cbf(cb,err,user.toObject(),newuser);
                  var html = "";
                  if(user["status"] == "active"){
                    html = "<B>"+ plainPassword +"</B>";
                  }else{
                    html = "<P>Please click the below link to activate your Account</P></br>" +
                   // "<a href='http://10.10.10.145:3100/api/activate/"+ user.activationcode+ "'>Activate my account</a>";
                    "<a href='http://localhost:3000/api/activate/"+ user.activationcode+ "'>Activate my account</a>";
                  }
                   sendmail(user,html);
                }); 
          }
      });
    }
  });
}; 


api.addBulkUser = function (user,cb) {
  //l.p('data frim apiObjects addbulkuser::::',user);
  if(user == 'undefined'){
    cb('No User Provided. Please provide valid user data.');
  }
 // l.p('username -- bulkuser :::: ' + user.username + 'emailid - bulkuser :::::::  ' + user.emailid);
  User.findOne({ 'username': user.username}, function(err, data) {
    var newuser = false;
    if(data != null){
      l.p('data already exist ::::',data);
      cbf(cb,err,"Username already exist",newuser);
      //cbf(cb,err,"User already exist",newuser);
    } else{
        User.findOne({ 'emailid':user.emailid }, function(err, data) {
          l.p('checking for user duplicacy ::: '+ data);
          if(data != null){
            l.p('EmailID alredy exist.... ' , newuser);
            // cbf(cb,err,data,newuser);
             cbf(cb,err,"EmailID already exist",newuser);
          } else{
              //  l.p('creating new userqwert');
                user = new User(user);
                newuser = true;
                l.p('creating new user', user);
                user.password  = genRandomString(16);
                var plainPassword = user.password;
                user.password = saltHashPassword(user.password);
                l.p('plainPassword ::::  ' + plainPassword);
                 user["status"] = "active";
                user.save(function (err) {
                  l.p('password zone12345');
                   // user["status"] = "active";
                  cbf(cb,err,user.toObject(),newuser);
                  var html = "";
                  html = "<B>"+ plainPassword +"</B>";
                  sendmail(user,html);
                }); 
            }
        });
      } 
  });
}; 

 
    function saltHashPassword(userpassword) {
          var salt = 'poin123!^&*LKJH';
          var passwordData = sha512(userpassword, salt);
          return passwordData;
      }

     var genRandomString = function(length){
        l.p('length :::: ' + length);
        return crypto.randomBytes(Math.ceil(length/2))
                .toString('hex') /** convert to hexadecimal format */
                .slice(0,length);   /** return required number of characters */
      };

      var sha512 = function(password,salt){
        l.p('password :::::: ' + password + '     salt ::::::::   ' + salt);
        var hash = crypto.createHmac('sha512', salt); /** Hashing algorithm sha512 */
        hash.update(password);
        var value = hash.digest('hex');
        return value;
      };


// PUT
api.editUser = function (id,updateData, cb) {
  User.findById(id, function (err, user) {
   
   if(updateData===undefined || user===undefined){
    return cbf(cb,'Invalid Data. Please Check user and/or updateData fields',null); 
  }
  
  
    if(typeof updateData["username"] != 'undefined'){
      user["username"] = updateData["username"];
    }
    
    if(typeof updateData["password"] != 'undefined'){
      user["password"] = updateData["password"];
    }
    
    if(typeof updateData["firstname"] != 'undefined'){
      user["firstname"] = updateData["firstname"];
    }
    
    if(typeof updateData["lastname"] != 'undefined'){
      user["lastname"] = updateData["lastname"];
    }
    
    if(typeof updateData["emailid"] != 'undefined'){
      user["emailid"] = updateData["emailid"];
    }
    
    if(typeof updateData["status"] != 'undefined'){
      user["status"] = updateData["status"];
    }

    if(typeof updateData["activationcode"] != 'undefined'){
      user["activationcode"] = updateData["activationcode"];
    }

  return user.save(function (err) {
    cbf(cb,err,user.toObject()); 
    }); //eo user.save
  });// eo user.find
};

// DELETE
api.deleteUser = function (id,cb) {
  return User.findById(id).remove().exec(function (err, user) {
   return cbf(cb,err,true);      
 });
};


// GET
api.findByEmail = function (emailid,cb) {
l.p('findByEmail called' + emailid);
  User.findOne({ 'emailid': emailid }, function(err, user) {
    l.p('findByEmail user '+ user);
    l.p('findByEmail err ' + err);
    cbf(cb,err,user);
  });
};

api.forgotpwd = function (user,cb) {

  if(user == undefined){
    cb('No User Provided. Please provide valid user data.');
    l.p('Please provide valid user data. Its undefined');
  }
   l.p('users for forgotpwd::: ' + user.emailid);
  User.findOne({'emailid':user.emailid, 'status':'active' }, function(err, user) {
    // l.p('successfully ' + user.emailid);
     if(err){
        return cbf(cb,'Details didnot match.',null); 
     }

    if(user == null){
      return cbf(cb,'Details didnot match.',null); 
    } else{
        l.p('creating new userqwert');
        user.password  = genRandomString(16);
        var plainPassword = user.password;
         l.p('creating new user password    ' + plainPassword);
        user.password = saltHashPassword(user.password);
        l.p('salted new user password  ', user.password);
         user.save(function (err) {
          l.p('password change');
          var html = "";
         // if(user["status"] == "active"){
            html = "<B>"+ plainPassword +"</B>";
          //}
           sendmail(user,html);
           cbf(cb,err,user.toObject());
        }); 
    }
  });
};

/*
========= [ SPECIAL METHODS ] =========
*/


//TEST
api.test=function (cb) {
  cbf(cb,false,{result:'ok'});
};


api.deleteAllUsers = function (cb) {
  return User.remove({},function (err) {
    cbf(cb,err,true);      
  });
};



/*
========= [ LOGIN METHOD ] =========
*/

// POST
api.userLogin = function (user,cb) {

  if(user == undefined){
    cb('No User Provided. Please provide valid user data.');
    l.p('Please provide valid user data. Its undefined');
  }
  // var passwordData = sha512(user.password, salt);
   passwordData = saltHashPassword(user.password);
   l.p('passwordData of login ::::: ' + passwordData);
  User.findOne({ 'username': user.username, 'password':passwordData, 'status':'active' }, function(err, user) {
    if(user == null){
      return cbf(cb,'Invalid username and password match.',null); 
    } else{
      //l.p('username and the password matched.', user);
      // send json token from here
      return cbf(cb,err,user); 
    }
  });
};


/*
========= [ UTILITY METHODS ] =========
*/

/** Callback Helper
 * @param  {Function} - Callback Function
 * @param  {Object} - The Error Object
 * @param  {Object} - Data Object
 * @return {Function} - Callback
 */
 
 var cbf=function(cb,err,data){
  if(cb && typeof(cb)=='function'){
    if(err) cb(err);
    else cb(false,data);
  }
};

//override function
 var cbf=function(cb,err,data,newuser){
  if(cb && typeof(cb)=='function'){
    if(err) cb(err);
    else cb(false,data,newuser);
  }
};

 var nodemailer = require("nodemailer");
var sendmail = function(user,html){
  l.p('send mail function called...' );
 // var nodemailer = require("nodemailer");
  l.p('cinjjh');
var smtpTransport = nodemailer.createTransport("SMTP",{
   service: "Gmail",  // sets automatically host, port and connection security settings
   auth: {
       user: "sweetchandrani1@gmail.com",
       pass: "Ct13011989"
   }
   //l.p('');
});

/*var transporter = nodemailer.createTransport({ 
    host: 'smtpout.secureserver.net', 
    port: 465, 
    auth: { user: 'Email address', pass: 'password' },
    secure: true
}); */


l.p("sending mail to :::: " + user.emailid);
smtpTransport.sendMail({  //email options
   from: "chandrani <sweetchandrani1@gmail.com>", // sender address.  Must be the same as authenticated user if using Gmail.
   to: user.emailid, // receiver
   subject: "InHouseDasboard", // subject
   html:html //"<a href='http://localhost:3000/api/activate/"+ user.activationcode+ "'>Activate my account</a>"
}, function(error, response){  //callback
   if(error){
      l.p("error :::: "  + error);
   }else{
       l.p("Message sent: " + response.message);
   }
   
   smtpTransport.close(); // shut down the connection pool, no more messages.  Comment this line out to continue sending emails.
});
};


module.exports = api;