// Module dependencies.
var crypto = require('crypto');
var express = require('express'),
router = express.Router(),
user = require('../apiObjects/user'),
l=require('../config/lib');

var modeluser = require('../models/user');

var jwt = require('jsonwebtoken');

var mongoose = require('mongoose'),
User = mongoose.models.User;

var secretCode = 'ASD&^%#lkmn567!@+:><&*hjkl';


var api = {};
// ALL
api.users = function (req, res) {
	var skip=null,limit=10;

	if(req.query.skip!=undefined)
		skip=req.query.skip;

	if(req.query.limit!=undefined)
		limit=req.query.limit;

	user.getAllUsers(skip,limit,function(err,data){
		if (err) {
			res.status(500).json(err);
		} else {
			res.status(200).json({users: data});
		}
	}); 
};


// POST
api.adduser = function (req, res) {
	l.p('hii enter to adduser' , req.body.user);
	user.addUser(req.body.user,function	(err,data,newuser){
		if(err) res.status(500).json(err);
		else {			
			if((data != null) && newuser == false ){
				/*if( data == "Username already exist"){
					res.status(202).json({users: newuser});
				} if(data == "EmailID already exist"){
					res.status(203).json({users: newuser});
				} */
				//l.p('check data username ::: ' + data.username);
				//l.p('check data emailid ::: ' + data.emailid);
				//l.p('User with the same name exist.', newuser);
				
				res.status(200).json({users: newuser});
			}
			else{
			l.p('new user.', newuser);
                   /* var token = generateJwt(data);
                    res.status(201);
                    res.json({
                      "token" : token
                    }); 
                    l.p('token ::::::: ' + token); */
                   // req.flash('success_msg','You are registered and can now login');
                  // req.flash('test', 'it worked');
                 //  res.send(JSON.stringify(req.flash('test')));
                    res.status(201).json(newuser);
                  //  res.status(201).json(req.flash('success_msg','You are registered and can now login'));
			}
		}
	});	
}; 

api.addBulkUser = function (req, res) {
	//l.p('hii enter to addBulkUser', req.body.users);
	//l.p('length of incoming array ::: ' + req.body.users.length);
	//l.p('length of incoming array 1::: ' + req.body.users[0].username);
	//l.p('length of incoming array  22::: ' + req.body.users[1]);
	var counter=0;
	req.body.users.forEach(function(arr) {
    	//l.p('eentryy : : :  ' , arr);
	

	// user.addBulkUser(req.body.users[0],function	(err,data){
	 user.addBulkUser(arr,function	(err,data,newuser){
	 	counter=counter+1;
	 	l.p("counter : " + counter + " , length : " + req.body.users.length);	
	 	if(req.body.users.length==counter)
	 	{
	 		l.p('response sent to ui');	
			if(err) res.status(500).json(err);
			else {			
				if((data != null) && newuser == false ){
					
					
					res.status(200).json({users: newuser});
				}
				else{
				l.p('new user. addbulk ::::: ', newuser);
	                   // req.flash('success_msg','You are registered and can now login');
	                  // req.flash('test', 'it worked');
	                 //  res.send(JSON.stringify(req.flash('test')));
	                    res.status(201).json(newuser);
	                  //  res.status(201).json(req.flash('success_msg','You are registered and can now login'));
				}
			}
		}
	});	

	});
}; 


  var generateJwt = function(user) {
  var expiry = new Date();
  expiry.setDate(expiry.getDate() + 2);
  l.p('secret code : ' + secretCode);
  return jwt.sign({
    _id: user._id,
    email: user.emailid,
    name: user.firstname + '' + user.lastname,
    username: user.username,
    exp: parseInt(expiry.getTime() / 1000),
  }, secretCode); // DO NOT KEEP YOUR SECRET IN THE CODE!
};

// GET
api.user = function (req,
 res) {
	var id = req.params.id;
	user.getUser(id,function(err,data){
		if (err) {
			res.status(404).json(err);
		} else {
			res.status(200).json({user: data});
		};
	}); 
};

// PUT
api.editUser = function (req, res) {
	var id = req.params.id;

	return user.editUser(id,req.body.user, function (err, data) {
		if (!err) {
			l.p("updated user");
			return res.status(200).json(data);
		} else {
			return res.status(500).json(err);
		}
		return res.status(200).json(data);   
	});

};

// DELETE
api.deleteUser = function (req, res) {
	var id = req.params.id;
	return user.deleteUser(id, function (err, data) {
		if (!err) {
			l.p("removed user");
			return res.status(204).send();
		} else {
			l.p(err);
			return res.status(500).json(err);
		}
	});
};

// DELETE All
api.deleteAllUsers = function (req, res) {
	return user.deleteAllUsers( function (err, data) {
		if (!err) {
			l.p("removed All user");
			return res.status(204).send();
		} else {
			l.p(err);
			return res.status(500).json(err);
		}
	});
};

var genRandomString = function(length){
        l.p('length :::: ' + length);
        return crypto.randomBytes(Math.ceil(length/2))
                .toString('hex') /** convert to hexadecimal format */
                .slice(0,length);   /** return required number of characters */
      };

api.checkUser = function (req,res) {
	l.p('checkuser called');
	var requestUser = req.body.user;
	l.p('requestUser' , requestUser);
	user.findByEmail(requestUser.emailid,function(err,data){
		if (err) {
			res.status(404).json(err);
		} else {
			if(data != null && data != undefined){
				//user exist
				var token = generateJwt(data);
	            res.status(200);
	            res.json({
	              "token" : token
	            });
	            l.p('token_checkuser::::::: ' + token);
			}else{
				l.p('need to create new user::   ');
				// send to add user
				//api.adduser(req,res);
				requestUser.password = genRandomString(8);;
				user.addUser(requestUser,function	(err,data,newuser){
					if(!err){
						var token = generateJwt(data);
			            res.status(200);
			            res.json({
			              "token" : token
			            });
			            l.p('token_checkuser::::::: ' + token);	
					}
				});
			}

			//res.status(200).json({user: data});
		}
	}); 
};


// LOGIN
api.userLogin = function (req, res) {
	var loginUser = req.body.user;
	return user.userLogin(loginUser, function (err, data) {
		if (!err) {
			l.p("user loggedin succesfully!!", data);
            var token = generateJwt(data);
            res.status(200);
            res.json({
              "token" : token
            });
            l.p('token ::::::: ' + token);
		} else {
			l.p('username and the password didnot match' + err);
			return res.status(500).json(err);
		}
	});
}; 


api.activate = function (req, res) {
	var activationcode = req.params.activationcode;
	user.activate(activationcode,function(err,data){
		if (err) {
			res.status(404).json(err);
		} else {
			res.status(200).json({user: data});
		}
	}); 
};

// POST
api.forgotpwd = function (req, res) {
	l.p(' to forgotpwd', req.body.user);
	user.forgotpwd(req.body.user,function	(err,data){
		l.p(' from forgotpwd', data);
		if(err) { l.p('errorrrr'); res.status(500).json(err); }
		else {			
			if((data != null) ){

				l.p('User with the same name exist.', data);
				//res.status(200).json({users: newuser});
				res.status(200).json({user: data});
			}
			else{
			l.p('new user.', newuser);
			l.p('pppppppppppppp...');
            	res.status(400).json({user: data});
			}
		}
	});	
}; 


/*
=====================  ROUTES  =====================
*/


router.post('/user',api.adduser);

router.route('/user/:id')
.get(api.user)
.put(api.editUser)
.delete(api.deleteUser);

//3rd party api.
router.post('/checkUser',api.checkUser);

router.post('/forgotpwd',api.forgotpwd);

router.route('/users')
.get(api.users)
.delete(api.deleteAllUsers);

router.post('/users/login',api.userLogin);

router.route('/activate/:activationcode')
.get(api.activate)

//bulk signup
router.post('/bulkuser',api.addBulkUser);

router.get('/users/test',function(req,res){
	return user.test(function (err, data) {
		res.status(200).json(data);
	});
});
 
module.exports = router;
