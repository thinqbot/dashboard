'use strict';

// Module dependencies.
var express = require('express'),
path = require('path'),
fs = require('fs'),
methodOverride = require('method-override'),
morgan = require('morgan'),
bodyParser = require('body-parser'),
errorhandler = require('errorhandler'),
cors=require('cors');
//gapi = require('./lib/gapi');

var flash = require('connect-flash');
var app = module.exports = exports.app = express();

//may comment it
var session = require('express-session');
var cookieParser = require('cookie-parser');
//var passport = require('passport');

//var jwt = require('jsonwebtoken');
//app.set('jwtTokenSecret', 'ASD&^%#lkmn567!@+:><&*hjkl');

app.locals.siteName = "Inhouse_server";

var accessLogStream = fs.createWriteStream(__dirname + '/../'+app.locals.siteName+'_access.log', {flags: 'a'})

app.use(cors());
app.use(morgan('dev'));
app.use(morgan('short',{stream: accessLogStream}));

 
app.use(cookieParser('secret'));
app.use(session({cookie: { maxAge: 60000 }}));
app.use(flash());


// configuring flash
/*app.configure(function() {
  app.use(express.cookieParser('keyboard cat'));
  app.use(express.session({ cookie: { maxAge: 60000 }}));
  app.use(flash());
});  */



// Connect to database
var db = require('./config/db');
app.use(express.static(__dirname + '/public'));
app.use('inhouse',express.static(__dirname + '/inhouse'));


// Bootstrap models
var modelsPath = path.join(__dirname, 'models');
fs.readdirSync(modelsPath).forEach(function (file) {
  require(modelsPath + '/' + file);
});

var env = process.env.NODE_ENV || 'development';

if ('development' == env) {
  app.use(errorhandler({
    //console.log('');
    dumpExceptions: true,
    showStack: true
  }));
  app.set('view options', {
    pretty: true
  });
}

if ('test' == env) {
  app.set('view options', {
    pretty: true
  });
  app.use(errorhandler({
    dumpExceptions: true,
    showStack: true
  }));
}

if ('production' == env) {
  app.use(errorhandler({
    dumpExceptions: false,
    showStack: false
  }));
}

app.set('view engine', 'html');
app.use(methodOverride());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(flash());

/*app.use(req,res,next){
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  next();
} */


/*app.use(function(req, res, next) {
  res.locals.messages = req.flash();
 //  res.locals.success_msg = req.flash('success_msg');
  next();
}); */
 
//may comment this para
app.use(session({
  secret: 'secret',
  resave: true,
  saveUninitialized: true
}));
//may comment this para


//app.use(passport.initialize());
//app.use(passport.session());


// Bootstrap routes
var routesPath = path.join(__dirname, 'routes');
fs.readdirSync(routesPath).forEach(function(file) {
  app.use('/', require(routesPath + '/' + file));
});

// Bootstrap api
var apiPath = path.join(__dirname, 'api');
fs.readdirSync(apiPath).forEach(function(file) {
  app.use('/api', require(apiPath + '/' + file));
});

// Start server
/*var port = process.env.PORT || 3000;
app.listen(port, function () {
  console.log('Express server listening on port %d in %s mode', port, app.get('env'));
}); */

var port = process.env.PORT || 3000;
console.log('port_app.js::::: ' + port);
app.listen(port, '0.0.0.0',function () {
  console.log('Express server listening on port %d in %s mode', port, app.get('env'));
  console.log('Express server listening on port %d in %s mode', port, app.get('env') , '0.0.0.0');
});

console.log('chandrani');
app.use(function(req,res,next){
  console.log('chandrani123456');
  res.locals.user = req.user || null;
  console.log('print ::::: ' + res.locals.user);
  next();
})