'use strict';
var mongoose = require('mongoose');

var config = {
  "db": "inhouse",  
  "host": "localhost",  
  // "host": process.env.MONGO_PORT_27017_TCP_ADDR,
  "user": "i",
  "pw": "i",
  // "port": process.env.MONGO_PORT_27017_TCP_PORT
  "port": 27017
};

console.log('config.port.length ::: ' + config.port.length);
console.log('config.port ::: ' + config.port);
console.log('NODEJS   ');
var port = (config.port.length > 0) ? ":" + config.port : '';
var login = (config.user.length > 0) ? config.user + ":" + config.pw + "@" : '';
var uristring = "mongodb://" + login + config.host + port + "/" + config.db;

var mongoOptions = { db: { safe: true } };

mongoose.Promise=global.Promise;

// Connect to Database
mongoose.connect(uristring, mongoOptions, function (err, res) {
  if(err){
    console.log('ERROR connecting to: ' + uristring + '. ' + err);
  }else{
    console.log('Successfully connected to: ' + uristring);
  }
});


exports.mongoose = mongoose;
