var request = require('supertest'),
express = require('express');

process.env.NODE_ENV = 'test';

var app = require('../app.js');
var _id = '';


describe('POST New User', function(){
  it('creates new user and responds with json success message', function(done){
    request(app)
    .post('/api/user')
    .set('Accept', 'application/json')
    .expect('Content-Type', /json/)
    .send({"user": {"username":"In the early 1410s several Fellows of Oriel took part in the disturbances accompanying Archbishop Arundel's attempt to stamp out Lollardy in the University; the Lollard belief that religious power and authority came through piety and not through the hierarchy of the Church particularly inflamed passions in Oxford, where its proponent, John Wycliffe, had been head of Balliol.","password":"Surrey is not recorded as ever having been an independent kingdom, but was at least a province that was under the control of different neighbours at different times.","firstname":"For practical reasons, breakfast was still eaten by working men, and was tolerated for young children, women, the elderly and the sick.","lastname":"The party embarked aboard HMAT Medic on 24 August, bound for Melbourne.","emailid":"Mating may last for nearly three hours."}})
    .expect(201)
    .end(function(err, res) {
      if (err) {
        throw err;
      }
      _id = res.body._id;
      done();
    });
  });
});

describe('GET List of Users', function(){
  it('responds with a list of user items in JSON', function(done){
    request(app)
    .get('/api/users')
    .set('Accept', 'application/json')
    .expect('Content-Type', /json/)
    .expect(200, done);
  });
});

describe('GET User by ID', function(){
  it('responds with a single user item in JSON', function(done){
    request(app)
    .get('/api/user/'+ _id )
    .set('Accept', 'application/json')
    .expect('Content-Type', /json/)
    .expect(200, done);
  });
});


describe('PUT User by ID', function(){
  it('updates user item in return JSON', function(done){
    request(app)
    .put('/api/user/'+ _id )
    .set('Accept', 'application/json')
    .expect('Content-Type', /json/)
    .send({ "user": { "title": "Hell Is Where There Are No Robots" } })    
    .expect(200, done);
  });
});

describe('DELETE User by ID', function(){
  it('should delete user and return 200 status code', function(done){
    request(app)
    .del('/api/user/'+ _id) 
    .expect(204, done);
  });
});