'use strict';

/**
 * @ngdoc function
 * @name inHouseDashboardApp.controller:ForgotpasswordCtrl
 * @description
 * # ForgotpasswordCtrl
 * Controller of the inHouseDashboardApp
 */
angular.module('inHouseDashboardApp')
  .controller('ForgotpasswordCtrl', function ($scope,$location,$http) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
      $scope.submit= function () {
    	console.log('forgotpassword...');
    
    if ($scope.forgotPwdForm.$valid) {      
   
		$http({
			method: "POST",
			//url: "api/forgotpwd",
			url: "http://localhost:3000/api/forgotpwd",
			 headers: {
          	 	'Content-Type': "application/json",
         	 },
			data: {
				"user":{
					//"username": $scope.username //,
					"emailid": $scope.email
				} 
			}
		}).then(function(res){
			console.log('res: ',res);
			if(res.status == 400){
				$scope.message = 'EmailID doesnot exist';
			}else if(res.status == 200){
				$location.path('/success');}
		},function(err){
			console.log('err: ',err);
			$scope.message = 'EmailID doesnot exist';
		});	 
		console.log('full users :::: ' + $scope.firstname);
    }
    else {
        //if form is not valid set $scope.forgotPwdForm.submitted to true     
        $scope.forgotPwdForm.submitted=true;   
        

}

   };
  });
