'use strict';

/**
 * @ngdoc function
 * @name inHouseDashboardApp.controller:LogoutCtrl
 * @description
 * # LogoutCtrl
 * Controller of the inHouseDashboardApp
 */
angular.module('inHouseDashboardApp')
  .controller('LogoutCtrl', function ($location,AuthenticateService) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    AuthenticateService.logout();
    //signOut();


    /*var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      console.log('User signed out.');
    }); */
 
   // gapi.auth.signOut();
    $location.path('/login');
  });
