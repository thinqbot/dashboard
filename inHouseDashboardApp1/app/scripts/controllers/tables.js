'use strict';

/**
 * @ngdoc function
 * @name inHouseDashboardApp.controller:TablesCtrl
 * @description
 * # TablesCtrl
 * Controller of the inHouseDashboardApp
 */
 angular.module('inHouseDashboardApp')
 	.controller('TablesCtrl', function ($scope,$http,$websocket,$location,$timeout,$sce,AuthenticateService) {
 	this.awesomeThings = [
 	'HTML5 Boilerplate',
 	'AngularJS',
 	'Karma'
 	];

 	console.log('Logged in::: ' +  AuthenticateService.isLoggedIn());

 	if(AuthenticateService.isLoggedIn() === false){
 		 $location.path('/login');
 	} 

	var wsURL = "",ws={};
	$scope.logtables=[];
	 $scope.sortReverse  = false;

	$scope.loadNew = function() {
		console.log('CREATING CONNECTION ..');	
    	$http({
			method:"GET",
			url:"/app.config"
		}).then(function success(response){
			wsURL = response.data.url;
			console.log('wsURL ::: '+ wsURL);
			ws = new WebSocket(wsURL);
			$scope.connection();
		},function error(response){
			var err = response.statusText;
			console.log('error loading :::: ' + err);
			alert('Connection failed.');
		}); 	
	};

	$scope.loadNew();
	$scope.connection = function(){
		ws.onopen = function(){  
			console.log('connection successfull... :::: ' + ws.readyState);
		};

		ws.onmessage = function(message) {
			var msg = angular.toJson(message.data);
			$scope.logtables.push(JSON.parse(message.data));
			$scope.$apply();

			var jsonValidate = JSON.parse(message.data);
			 $scope.jsonHTML = JSON.parse(message.data);
        	var messageRes = jsonValidate.message;
			
		$scope.jsonFormat = function(){
			 var html = $sce.trustAsHtml($scope.jsonHTML.message);
			 $scope.myString = "'GET'";
			 var splitedMessage = $scope.jsonHTML.message.split("esponse:")[1];//.split(",")[0];
			 console.log('splitedMessage..:::: ' + splitedMessage);
			 $scope.quote = splitedMessage.toLowerCase().replace(/'/g,'"').replace('none','null').replace(/{u"}/g,'');//.
			 console.log('$scope.quote :::::  ' + $scope.quote);
			 console.log('quote..:::: ' + $scope.quote);
			 if(($scope.quote.includes('",')) || ($scope.quote.includes('",')))

			try {
			    JSON.parse($scope.quote);
			} catch (e) {
		    	console.log('It is not in JSON format.');
		        return false;
		    }
		    	console.log('It is in JSON format.');
		    	return true;
			}
		};
	};

	$scope.clear = function(){
	var divContainer = document.getElementById("tbody_id");
	console.log('divContainer :::::: + ' + divContainer);
	divContainer.innerHTML = "";
	console.log('divContainer.innerHTML ::::: ' + divContainer.innerHTML);
	$scope.loadNew();
	console.log('divContainer.innerHTML11 ::::: ' + divContainer.innerHTML);
}
$scope.loadNew();

});