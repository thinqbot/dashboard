'use strict';

/**
 * @ngdoc function
 * @name inHouseDashboardApp.controller:TablesCtrl
 * @description
 * # TablesCtrl
 * Controller of the inHouseDashboardApp
 */
 angular.module('inHouseDashboardApp')
 .controller('TablesCtrl11', function ($scope,$http,$websocket,$timeout) {
 	this.awesomeThings = [
 	'HTML5 Boilerplate',
 	'AngularJS',
 	'Karma'
 	];

    // data from http://qube.local/w/v1/ips
//"{"message": null, "payload": {"ips": ["aaaa::212:4b00:60d:b0be", "aaaa::212:4b00:63a:7339"]}, "success": true}"

 // var ws = $websocket.$new('ws://qube.local:9090/_ws');  
 /*{
  "levelname": "DEBUG",
   "asctime": "2016-10-07 19:10:31,542", 
   "message": "Response: {'message': None, 'payload': {'ips': [u'aaaa::212:4b00:60d:b0be', u'aaaa::212:4b00:63a:7339']}, 'success': True}", 
   "lineno": 33, 
   "pathname": "./src/api/v1/libraries/customresponse.py"
}
*/

	var wsURL = "";
	var table = document.getElementById("table_id"); 
	 var divContainer = document.getElementById("showData");

	$http({
		method:"GET",
		url:"/app.config"
	}).then(function success(response){
		wsURL = response.data.url;
		console.log('wsURL ::: '+ wsURL);
		},function error(response){
			var err = response.statusText;
			console.log('error loading :::: ' + err);
			alert('Connection failed.');
	})

$scope.refresh = function(){
	//window.onload = function() {
		return $timeout(function() {
			console.log('window.onload is called... ' );

		if(wsURL === ""){
			//alert('Please check the websocket Server ' + wsURL +' to connect again.');
			return;
		}

		try{
		var ws = new WebSocket(wsURL);
		console.log('wsURL ::::: ' + wsURL);
		console.log('wsU ::::: ' + ws);
		/*if(ws == '[object WebSocket]'){
			throw new Error("Not a number");
			alert('sad');
		} */
		//alert('Please check the websocket Server ' + wsURL +' to connect again.');
		} catch (e){
			alert('error ::: ' + e);
		}

		ws.onopen = function(){   
			console.log('connection successfull... :::: ' + ws.readyState);
		};

		ws.onmessage = function(message) {
			$scope.device = angular.toJson(message.data);
			var msg = JSON.parse(message.data);
        	
			var table_arr = [];
			var table_arr_data = [];
			var levelname = msg.levelname;
			//var message = msg.message.split("message':")[1].split(",")[0];
			var pathname = msg.pathname; 
			var message = msg.message;
			var asctime = msg.asctime;
			
			table_arr.push(['AscTime']);
			table_arr.push(['LevelName']); //1
			table_arr.push(['Message']); //2 
			
			table_arr_data.push([asctime]);
			table_arr_data.push([levelname]);
			table_arr_data.push([message]);
			
// CREATE DYNAMIC TABLE.
			var br = document.createElement("br");
			var newTable=false;
			//var table = document.getElementById("table_id"); 
			if (table == null || table == void 0){
				table = document.createElement("table");
				table.className = "table table-striped jambo_table bulk_action";
				table.id = "table_id";
				newTable=true;
			}


// CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.

			var tr = table.insertRow(-1);                   // TABLE ROW.
			tr.className = "even pointer";
			tr.style.border = '1px solid black';
			var select = document.createElement("select");
			//select.id = "levelname_select";
			var option = document.createElement("option");
			  
			if(newTable==true){
				for (var i = 0; i < table_arr.length; i++) {
					var th = document.createElement("th");   
			        // TABLE HEADER.
			        th.className = "column-title";
			        th.style.border = '1px solid black';
			        th.innerHTML = table_arr[i];
			        if(th.innerHTML === 'LevelName'){ /* "url": "ws://qube.local:9090/_ws" */
			        	
			        	th.innerHTML = table_arr[i] +' &nbsp;' + '<select id = "levelname_select"><option id="levelname_option"></option></select>'; 
			        	//th.innerHTML = table_arr[i] +' &nbsp; '+ document.getElementById("levelname_select") ;//;+ '<option>select the levelname</option>'; 
			        } 
    /* <option value="" disabled="disabled" selected="selected">Please select a name</option>
    <option value="1">One</option>
    <option value="2">Two</option>'; */
					tr.appendChild(th); 
				}
			}
			table.appendChild(tr);
			divContainer.appendChild(table);

		//	tr = table.insertRow(-1);
			tr = table.insertRow(1);
			var counter = 0;
			    for (var i = 0; i < table_arr_data.length; i++) {
			      	var td = document.createElement("td"); 
			      		/*$("#table_id > tr").click(function(){
        				$(this).toggleClass("zoomText");
   						 }); */
			        	td.className = "column-title";
			        	td.style.border = '1px solid black';
			        	td.innerHTML = table_arr_data[i];
			        //	console.log('table_arr_data[ ' + i  + '] ' + table_arr_data[i]);
			        	if(td.innerHTML.toUpperCase() === 'ERROR'){
			        		td.id="redText";
			        	}
			        		
			        	var exist = $("#levelname_select option[value='"+ table_arr_data[1] +"']").length
						if (exist<=0)
							{
								var select1 = document.getElementById("levelname_select");
								var option1 = document.createElement("option");
								option1.value=table_arr_data[1];
								option1.text=table_arr_data[1];
								select1.appendChild(option1);
									console.log('option1 :::: ' + option1);
							}

							//exist = $("#levelname_select option[value='"+ table_arr_data[i][1] +"']").length
							exist = $("#levelname_select option[value='"+ table_arr_data[1] +"']").length
							if (exist<=0)
							{
								var select2 = document.getElementById("levelname_select");
								var option2 = document.createElement("option");
								//option2.value=table_arr_data[i][1];
								//option2.text=table_arr_data[i][1];
								option2.value=table_arr_data[1];
								option2.text=table_arr_data[1];
								select2.appendChild(option2);
									console.log('option2 :::: ' + option2);
							}
 
			        	$scope.toggle = function(){
							$("#table_id > tr").toggleClass("zoomText");
							//$("#table_id > tr").click(function(){
      //  $(this).toggleClass("zoomText");
    //});
						}  

						
						//$(document).ready(function(){
    					//$("#table_id > tr").click(function(){
						//});
			        	tr.appendChild(td);
			        }
			     // var input = document.getElementById("levelname_select");
 // var filter = input.value.toUpperCase();
 //var  table = document.getElementById("table_id");
var tr = table.getElementsByTagName("tr");
var filter = levelname_select.value.toUpperCase();

			        	$('#levelname_select').change(function () {
			         	console.log('the vale has been changed');
			         	//console.log('tr.length :::: ' + td.length);
			         	//for (i = 0; i < tr.length; i++) {
			         	for (i = 0; i < 40; i++) {
			         		console.log('t');
						    td = tr[i].getElementsByTagName("td")[0];
						    console.log('tddd :::: ' + td);
						    if (td) {
						      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
						        tr[i].style.display = "";
						      } else {
						        tr[i].style.display = "none";
						      }
						    }
						  }
			         });

		       
 // FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
			      //  var divContainer = document.getElementById("showData");
			       // divContainer.className = "table-responsive";
			       // divContainer.innerHTML = "";
			         //divContainer.appendChild(br);
			        // $("table").append(header);
			        divContainer.appendChild(table);
			        //divContainer.appendChild(header); 
			}
		}, 1000);
	//};
};

$scope.clear = function(){
	var divContainer = document.getElementById("showData");
	divContainer.innerHTML = "";
}

/*$scope.zoom = function(){
	//alert('Zoom ...');
	var divContainer = document.getElementById("showData");
	var tr = document.createElement("tr"); 
	tr.id="zoomText";
	if ($scope.zoomIndex !== val) {
    $scope.zoomIndex = val;
  } else {
    $scope.zoomIndex = 999;
  }  
  divContainer.appendChild(table);
}  */
});
