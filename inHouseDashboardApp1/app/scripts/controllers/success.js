'use strict';

/**
 * @ngdoc function
 * @name inHouseDashboardApp.controller:SuccessCtrl
 * @description
 * # SuccessCtrl
 * Controller of the inHouseDashboardApp
 */
angular.module('inHouseDashboardApp')
  .controller('SuccessCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
