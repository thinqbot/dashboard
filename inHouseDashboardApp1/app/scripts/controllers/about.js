'use strict';

/**
 * @ngdoc function
 * @name angularIndexTestApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the angularIndexTestApp
 */
angular.module('inHouseDashboardApp') 
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
