'use strict';

/**
 * @ngdoc function
 * @name inHouseDashboardApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the inHouseDashboardApp
 */
angular.module('inHouseDashboardApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

/*console.log("testing NodeJS implementation for the Server");

var http = require("http");
http.createServer(function(request,response){
	response.writeHead(200, {'Content-Type': 'text/plain'});
	response.end('Hello World!! \n');
}).listen(8081);

console.log('Server running at http://127.0.0.1:8081/'); */


/*var static = require('node-static');
 
// 
// Create a node-static server instance to serve the './public' folder 
// 
var file = new static.Server('./public');
 
require('http').createServer(function (request, response) {
    request.addListener('end', function () {
        // 
        // Serve files! 
        // 
        response.end('Hello World!! \n');
        file.serve(request, response);
    }).resume();
}).listen(8080); */