'use strict';

/**
 * @ngdoc function
 * @name inHouseDashboardApp.controller:ChartjsCtrl
 * @description
 * # ChartjsCtrl
 * Controller of the inHouseDashboardApp
 */
angular.module('inHouseDashboardApp')
  .controller('ChartjsCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
