'use strict';

/**
 * @ngdoc function
 * @name inHouseDashboardApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the inHouseDashboardApp
 */
angular.module('inHouseDashboardApp')
  .controller('LoginCtrl', function ($scope,$location,$http,$rootScope,AuthenticateService) {
  //  .controller('LoginCtrl', function ($scope,$location,loginService) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    /*function start() {
      console.log('start method called');
      gapi.load('auth2', function() {
        auth2 = gapi.auth2.init({
          client_id: '560883349067-1c3050mshcb18ft7eugul772727tovuj.apps.googleusercontent.com',
          // Scopes to request in addition to 'profile' and 'email'
          //scope: 'additional_scope'
           scope : 'https://www.googleapis.com/auth/plus.login'
        });
      });
    };

    $scope.googleSignin = function(){
       console.log('APi of google');
    // signInCallback defined in step 6.
    //start();
    auth2.grantOfflineAccess({'redirect_uri': 'postmessage'}).then(signInCallback);
    } */


    /*  $scope.gsignin = function onSignIn(googleUser) {
        // Useful data for your client-side scripts:
        var profile = googleUser.getBasicProfile();
         var id_token = googleUser.getAuthResponse().id_token;
         console.log("id_token: " + id_token);
        console.log("ID: " + profile.getId()); // Don't send this directly to your server!
        console.log('Full Name: ' + profile.getName());
        console.log('Given Name: ' + profile.getGivenName());
        console.log('Family Name: ' + profile.getFamilyName());
        console.log("Image URL: " + profile.getImageUrl());
        console.log("Email: " + profile.getEmail());

        // The ID token you need to pass to your backend:
        var id_token = googleUser.getAuthResponse().id_token;
        console.log("ID Token: " + id_token);
      }; */




  /*  var xhr = new XMLHttpRequest();
xhr.open('POST', 'https://yourbackend.example.com/tokensignin');
xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
xhr.onload = function() {
  console.log('Signed in as: ' + xhr.responseText);
};
xhr.send('idtoken=' + id_token); */





    $scope.googleSignin=function() {
       console.log('googleSignin method called');
       var client_id="560883349067-ahulf9idbf3uslgqgeabdldedfh0tvvi.apps.googleusercontent.com";
       var scope="https://www.googleapis.com/auth/plus.login";
       //var redirect_uri="http://localhost:9000/oauth2callback";
       //var redirect_uri="http://localhost:3000";
       var redirect_uri="http://localhost:9000";
       var response_type="token";
       var url="https://accounts.google.com/o/oauth2/auth?scope="+scope+"&client_id="+client_id+"&redirect_uri="+redirect_uri+
       "&response_type="+response_type;
      // var url ="https://accounts.google.com/o/oauth2/auth?client_id=292824132082.apps.googleusercontent.com&immediate=false&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile&include_granted_scopes=false&hl=en_US&proxy=oauth2relay868491881&redirect_uri=postmessage&origin=https%3A%2F%2Fdevelopers.google.com&response_type=token&gsiwebsdk=1&state=696786501%7C0.3119910629&authuser=0&jsh=m%3B%2F_%2Fscs%2Fapps-static%2F_%2Fjs%2Fk%3Doz.gapi.en.ZTWSkXpdD-c.O%2Fm%3D__features__%2Fam%3DEQ%2Frt%3Dj%2Fd%3D1%2Frs%3DAGLTcCPlLSEUo3HS2F8u16UJ3dFU-V3TDg";
      window.location.replace(url);
     };


    $scope.submit = function(){
 if ($scope.LoginForm.$valid) {      
      //form is valid
   	console.log('Login continue...');
    console.log('$scope.login.username : ' + $scope.login.username);
     console.log('$scope.login.password : ' + $scope.login.password);
    //	var loginCredentials = $scope.login;
      $http({
      method: "POST",
      url: "http://localhost:3000/api/users/login",
      //url: "api/users/login",
       headers: {
              "Content-Type": "application/json",
           },
      data: {
        "user":{
          "username": $scope.login.username,
          "password": $scope.login.password
        } 
      }
    }).then(function success(res){ //then(function(res){
     // console.log('res: ',res.data.);
      // console.log('response is successfull for UI');
     
       $scope.login = res.data;
      // $localStorage.login = $scope.login; 
       console.log('response for data ::::: ' + $scope.login._id + "    username :::::  " + $scope.login.username);
       console.log('token for login ::: ' + res.data.token);
       $scope.username = AuthenticateService.currentUser.payload;
       AuthenticateService.saveToken(res.data.token);
       $location.path('/tables');
    }, function error(err){//function(err){
      console.log('err: ',err);
      //alert('Please Provide valid usernam.');
      $scope.message = 'Username and Password does not match.';
       console.log('response is not at all successfull for UI');
    });  

     }
    else {
        //if form is not valid set $scope.addContact.submitted to true     
        $scope.LoginForm.submitted=true;   
        

}

  //  console.log('full users :::: ' + $scope.register.username);
     /* console.log('loginCredentials :::::: ' + loginCredentials.username);
      if(loginCredentials.username === 'admin' && loginCredentials.password === 'admin'){
        console.log('welcome ::: ' + loginCredentials.username);
      }  */

     /*  //$localStorage.LocalMessage('hello world');
      //console.log('Login continue...');
      var isAuthenticate = false;
      var loginCredentials = $scope.login;
      //console.log('loginCredentials.username :::::' + loginCredentials.username);
      //console.log('loginCredentials.password :::::' + loginCredentials.password);
      isAuthenticate = loginService.checkCredentials(loginCredentials.username,loginCredentials.password);

      if(isAuthenticate === true){
        $location.path('/tables');
        console.log('called isAuthenticate() :::: ' + isAuthenticate);
        //console.log('checkCredentials');
      } else {
        console.log('Please login..!!');
        alert('Provide credentials');
      } */
console.log('$scope.data /......res.data;' + $scope.login.username);
    }; 

  });