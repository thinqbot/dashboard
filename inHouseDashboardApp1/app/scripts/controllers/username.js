'use strict';

/**
 * @ngdoc function
 * @name inHouseDashboardApp.controller:UsernameCtrl
 * @description
 * # UsernameCtrl
 * Controller of the inHouseDashboardApp
 */
angular.module('inHouseDashboardApp')
  .controller('UsernameCtrl', function (AuthenticateService) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];


    $scope.username = AuthenticateService.currentUser().username;
    //console.log('iso::: ' + iso);
  });
