'use strict';

/**
 * @ngdoc function
 * @name inHouseDashboardApp.controller:BulksuccessCtrl
 * @description
 * # BulksuccessCtrl
 * Controller of the inHouseDashboardApp
 */
angular.module('inHouseDashboardApp')
  .controller('BulksuccessCtrl', function ($scope,$location,AuthenticateService) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    console.log('Logged in::: ' +  AuthenticateService.isLoggedIn());

 	if(AuthenticateService.isLoggedIn() === false){
 		 $location.path('/login');
 	} 


  });
