'use strict';

/**
 * @ngdoc function
 * @name inHouseDashboardApp.controller:RegisterCtrl
 * @description
 * # RegisterCtrl
 * Controller of the inHouseDashboardApp
 */
angular.module('inHouseDashboardApp')
  .controller('RegisterCtrl', function ($scope,$http,$location,$routeParams,AuthenticateService) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];


   //  <script>
 
   		window.onSignIn = onSignIn;
      function onSignIn(googleUser) {
        // Useful data for your client-side scripts:
        var profile = googleUser.getBasicProfile();
         var id_token = googleUser.getAuthResponse().id_token;
        console.log("ID: " + profile.getId()); // Don't send this directly to your server!
        console.log('Full Name: ' + profile.getName());
        console.log('Given Name: ' + profile.getGivenName());
        console.log('Family Name: ' + profile.getFamilyName());
        console.log("Image URL: " + profile.getImageUrl());
        console.log("Email: " + profile.getEmail()); 
        

        // The ID token you need to pass to your backend:
        var id_token = googleUser.getAuthResponse().id_token;
        console.log("ID Token: " + id_token);

        var username = profile.getEmail();
        var name = profile.getName();
        var firstname = name.split(' ')[0];
        var lastname = name.split(' ')[1];
        var emailid = profile.getEmail();
        var password = '';

        $http({
			method: "POST",
			//url: "api/checkUser",
			url: "http://localhost:3000/api/checkUser",

			headers: {
          	 	'Content-Type': "application/json",
         	 },
			data: {
				"user":{
					"username": username,
					"firstname":firstname,
					"lastname": lastname,
					"emailid": emailid,
					"password": password,
					"status":"active"
				} 
			}
		}).then(function(res){
			console.log('res: ',res);
			console.log('sucessy reverted from usercheck');
			$scope.login = res.data;
      // $localStorage.login = $scope.login; 
       console.log('response for data ::::: ' + $scope.login._id + "    username :::::  " + $scope.login.username);
     //  console.log('token for login ::: ' + res.data.token);
      // $scope.username = AuthenticateService.currentUser.payload;
       AuthenticateService.saveToken(res.data.token);
       $location.path('/tables');
			/*if(res.status == 200){
				console.log('Token from server : ' + res.data.token);
			}else */
			/*if(res.status == 201){
			//$location.path('/login');
				$http({
				      method: "POST",
				      url: "http://localhost:3000/api/users/login",
				       headers: {
				              "Content-Type": "application/json",
				           },
				      data: {
				        "user":{
				          "username": username,
				          "password": password
				        } 
				      }
				    }).then(function success(res){ 
				     
				     //  $scope.login = res.data;
				      // $localStorage.login = $scope.login; 
				       //console.log('response for data ::::: ' + $scope.login._id + "    username :::::  " + $scope.login.username);
				       //console.log('token for login ::: ' + res.data.token);
				       $scope.username = AuthenticateService.currentUser.payload;
				       AuthenticateService.saveToken(res.data.token);
				       $location.path('/tables');
				    }, function error(err){
				      console.log('err: ',err);
				       console.log('response is not at all successfull for UI');
				    });  


			} */
		},function(err){
			console.log('err: ',err);
		});	 

      };
    //</script> 

    /*$scope.doregister = function(){ */
    $scope.doregister= function () {
    	console.log('register...');
    	var registerCredentials = $scope.register;
    	/*$http({
			method:"GET",
			url:"http://localhost:3000/api/users"
		}).then(function success(response){
			var users = response.data.users;
			// var message = response.users;
			for(var i=0; i<users.length;i++){
         	console.log('users::::  '+ users[i].username + users[i].emailid);
         }
		},function error(response){
			var err = response.statusText;
			console.log('error loading :::: ' + err);
			alert('Connection failed.');
		}); */

		//console.log('emailid: ',$scope.register.username);


    if ($scope.registerForm.$valid) {      
      //form is valid

		$http({
			method: "POST",
			//url: "api/user",
			url: "http://localhost:3000/api/user",
			 headers: {
          	 	'Content-Type': "application/json",
         	 },
			data: {
				"user":{
					"username": $scope.register.username,
					"firstname":$scope.register.firstname,
					"lastname": $scope.register.lastname,
					"emailid": $scope.register.email,
					"password": $scope.register.password
				} 
			}
		}).then(function(res){
			console.log('res: ',res);
			/*if(res.status == 202){
				console.log('Token from server : username exist ' + res.data.token);
				//alert('User already exist');
				$scope.message_username = 'Username already exists';
			}else if(res.status == 203){
				console.log('Token from server : emailid exist' + res.data.token);
				//alert('User already exist');
				$scope.message_email = 'EmailID already exists';
			}*/

			if(res.status == 200){
				$scope.message = 'User already exists';
			}else if(res.status == 201){
				 //var msg=$routeParams.message;
				 //console.log('From nodejs to angularsj'+ msg);
			$location.path('/success');}
		},function(err){
			console.log('err: ',err);
		});	 
		console.log('full users :::: ' + $scope.register.firstname);
    }
    else {
        //if form is not valid set $scope.addContact.submitted to true     
        $scope.registerForm.submitted=true;   
        

}

   };

  });
