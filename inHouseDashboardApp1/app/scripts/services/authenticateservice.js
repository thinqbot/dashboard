'use strict';

/**
 * @ngdoc service
 * @name inHouseDashboardApp.AuthenticateService
 * @description
 * # AuthenticateService
 * Service in the inHouseDashboardApp.
 */
angular.module('inHouseDashboardApp')
  .service('AuthenticateService', authentication);
    // AngularJS will instantiate a singleton by calling "new" on this function

  authentication.$inject = ['$http', '$window'];


  function authentication ($http, $window) {

    var saveToken = function (token) {
      $window.localStorage['token'] = token;
    };

    var getToken = function () {
      return $window.localStorage['token'];
    };

    var logout = function() {
      $window.localStorage.removeItem('token');
    };

    var isLoggedIn = function() {
	  var token = getToken();
	  var payload;

	  if(token){
	    payload = token.split('.')[1];
	    payload = $window.atob(payload);
	    payload = JSON.parse(payload);

	    return payload.exp > Date.now() / 1000;
	  } else {
	    return false;
	  }
	};

	var currentUser = function() {
	  if(isLoggedIn()){
	    var token = getToken();
	    var payload = token.split('.')[1];
	    payload = $window.atob(payload);
	    payload = JSON.parse(payload);
	    return {
	      email : payload.email,
	      name : payload.name,
	      username : payload.username
	    };
	  }
	};

    return {
      saveToken : saveToken,
      getToken : getToken,
      logout : logout,
      isLoggedIn : isLoggedIn,
      currentUser : currentUser
    };
};