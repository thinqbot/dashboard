'use strict';

/**
 * @ngdoc overview
 * @name inHouseDashboardApp
 * @description
 * # inHouseDashboardApp
 *
 * Main module of the application.
 */
angular
  .module('inHouseDashboardApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'angular.filter',
    'ngTouch',
    'ngWebsocket'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        /*templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'  */
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl',
        controllerAs: 'login',
        public: true,
        login: true 
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/tables', {
        templateUrl: 'views/tables.html',
        controller: 'TablesCtrl',
        controllerAs: 'tables'
      })
      .when('/chartjs', {
        templateUrl: 'views/chartjs.html',
        controller: 'ChartjsCtrl',
        controllerAs: 'chartjs'
      })
      .when('/login', {
       templateUrl: 'views/login.html',
        controller: 'LoginCtrl',
        controllerAs: 'login',
        public: true,
        login: true
      }) 
      .when('/navController', {
        templateUrl: 'views/navcontroller.html',
        controller: 'NavcontrollerCtrl',
        controllerAs: 'navController'
      })
      .when('/register', {
        templateUrl: 'views/register.html',
        controller: 'RegisterCtrl',
        controllerAs: 'register'
      })
      .when('/forgotpassword', {
        templateUrl: 'views/forgotpassword.html',
        controller: 'ForgotpasswordCtrl',
        controllerAs: 'forgotpassword'
      })
      .when('/success', {
        templateUrl: 'views/success.html',
        controller: 'SuccessCtrl',
        controllerAs: 'success'
      })
      .when('/logout', {
        templateUrl: 'views/logout.html',
        controller: 'LogoutCtrl',
        controllerAs: 'logout'
      })
      //
      /*.when('/access_token=:accessToken', {
          template: '',
          controller: function ($location,$rootScope) {
            var hash = $location.path().substr(1);
           
           var splitted = hash.split('&');
           var params = {};
 
           for (var i = 0; i < splitted.length; i++) {
             var param  = splitted[i].split('=');
             var key    = param[0];
             var value  = param[1];
             params[key] = value;
             $rootScope.accesstoken=params;
             console.log('key:: ' + key + 'value :::: ' + value);
           }
           console.log(params);
           $location.path("/tables");
         }
       })  */
      //
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/bulklogin', {
        templateUrl: 'views/bulklogin.html',
        controller: 'BulkloginCtrl',
        controllerAs: 'bulklogin'
      })
      .when('/bulksuccess', {
        templateUrl: 'views/bulksuccess.html',
        controller: 'BulksuccessCtrl',
        controllerAs: 'bulksuccess'
      })
      .when('/bulksuccess', {
        templateUrl: 'views/bulksuccess.html',
        controller: 'BulksuccessCtrl',
        controllerAs: 'bulksuccess'
      })
      .otherwise({
        //redirectTo: '/'
        redirectTo: '/login'
      });
  });

  /*window.onload = function() {

  var form = document.getElementById('message-form');
 }; */