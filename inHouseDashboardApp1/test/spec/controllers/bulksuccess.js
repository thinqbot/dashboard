'use strict';

describe('Controller: BulksuccessCtrl', function () {

  // load the controller's module
  beforeEach(module('inHouseDashboardApp'));

  var BulksuccessCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    BulksuccessCtrl = $controller('BulksuccessCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(BulksuccessCtrl.awesomeThings.length).toBe(3);
  });
});
