'use strict';

describe('Controller: ChartjsCtrl', function () {

  // load the controller's module
  beforeEach(module('inHouseDashboardApp'));

  var ChartjsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ChartjsCtrl = $controller('ChartjsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ChartjsCtrl.awesomeThings.length).toBe(3);
  });
});
