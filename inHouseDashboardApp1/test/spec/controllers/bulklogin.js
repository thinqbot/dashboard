'use strict';

describe('Controller: BulkloginCtrl', function () {

  // load the controller's module
  beforeEach(module('inHouseDashboardApp'));

  var BulkloginCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    BulkloginCtrl = $controller('BulkloginCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(BulkloginCtrl.awesomeThings.length).toBe(3);
  });
});
