'use strict';

describe('Service: AuthenticateService', function () {

  // load the service's module
  beforeEach(module('inHouseDashboardApp'));

  // instantiate service
  var AuthenticateService;
  beforeEach(inject(function (_AuthenticateService_) {
    AuthenticateService = _AuthenticateService_;
  }));

  it('should do something', function () {
    expect(!!AuthenticateService).toBe(true);
  });

});
